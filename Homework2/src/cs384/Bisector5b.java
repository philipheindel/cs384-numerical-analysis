package cs384;

public class Bisector5b {

	public static double bisect(double a, double b, double epsilon) {
		double c;

		if (function(a) * function(b) >= 0) {
			System.out.println("a or b are incorrect");
			return -1.0;
		}

		c = a;

		for (int i = 0; (b - a) >= epsilon; i++) {
			c = (a + b) / 2;

			if (function(c) == 0.0) {
				break;
			} else if (function(c) * function(a) < 0) {
				b = c;
			} else {
				a = c;
			}
			System.out.println((i + 1) + ":\t" + c);
		}
		return c;
	}

	private static double function(double x) {
		return Math.exp(x) - (x * x) + (3 * x) - 2;
	}

}
