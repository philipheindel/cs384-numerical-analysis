package cs384;

public class Main {

	public static void main(String[] args) {
		System.out.println("Problem: (e^x) - (x^2) + (3*x) - 2 = 0 for 0 <= x <= 1");
		System.out.println("Bisection with accuracy 10^-5");
		System.out.printf("Bisection: %.5f\n", Bisector5b.bisect(0, 1, 0.01));
		System.out.println("Bisection with accuracy 10^-10");
		System.out.printf("Bisection: %.10f\n", Bisector5b.bisect(0, 1, 0.001));
		System.out.println("sqrt(3) approx. = " + Math.sqrt(3.0));
		System.out.println("Bisection with accuracy 10^-5");
		System.out.printf("Bisection: %.5f\n", Bisector14.bisect(1, 2, 0.01));
	}

}
