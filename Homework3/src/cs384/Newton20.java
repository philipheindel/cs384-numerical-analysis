package cs384;

public class Newton20 {

	private static double epsilon;
	
	public Newton20(double epsilon) {
		Newton20.epsilon = epsilon;
	}

	private double f(double x) {
		return (-2 * Math.pow(x, -1)) + Math.pow(x, -2) + Math.pow(x, 2) - (4 * x) + 8;
	}

	private double fdx1(double x) {
		return (2 * Math.pow(x, -2)) - (2 * Math.pow(x, -3)) + (2 * x) - 4;
	}

	public double calculate(double x) {
		double h = f(x) / fdx1(x);
		int i = 0;
		while (Math.abs(h) >= Newton20.epsilon) {
			h = f(x) / fdx1(x);
			
			x -= h;
			if (i % 10 == 0) {
				System.out.println(i + ": " + x);
			}
			i++;
		}
		System.out.println(i + ": " + x);
		return x;
	}
	
	

}
