package cs384;

public class Newton {

	private static double epsilon;
	
	public Newton(double epsilon) {
		Newton.epsilon = epsilon;
	}

	private double f(double x) {
		return Math.exp(x) + Math.pow(2, -x) + (2 * Math.cos(x)) - 6;
	}

	private double fdx1(double x) {
		return Math.exp(x) - Math.pow(2, -x) * Math.log(2) - (2 * Math.sin(x));
	}

	public double calculate(double x) {
		double h = f(x) / fdx1(x);
		int i = 0;
		while (Math.abs(h) >= Newton.epsilon) {
			h = f(x) / fdx1(x);
			
			x -= h;
			System.out.println((++i) + ": " + x);
		}
		return x;
	}
	
	

}
