package cs384;

public class Main {

	public static void main(String[] args) {
		double x = 1.5;
		double accuracy = Math.pow(10, -1);
		printNewton6a(x,accuracy);
		accuracy = Math.pow(10, -3);
		printNewton6a(x,accuracy);
		accuracy = Math.pow(10, -5);
		printNewton6a(x,accuracy);
		accuracy = Math.pow(10, -7);
		printNewton6a(x,accuracy);
		accuracy = Math.pow(10, -11);
		printNewton6a(x,accuracy);
		
		accuracy = Math.pow(10, -4);
		printNewton20(x,accuracy);
	}
	
	private static void printNewton6a(double x, double accuracy) {
		Newton6a n = new Newton6a(accuracy);
		System.out.println("Problems:\nf(x) = (e^x) + (2^-x) + (2 * cos(x)) - 6 for 1 <= x <= 2");
		System.out.println("f'(x) = (e^x) - (2^-x) * log(2) - (2 * sin(x)) for 1 <= x <= 2");
		System.out.println("\nx = " + x + "\naccuracy = " + accuracy + "\n");
		n.calculate(x);
		System.out.println("\n========================================\n");
	}
	
	private static void printNewton20(double x, double accuracy) {
		Newton20 n = new Newton20(accuracy);
		System.out.println("Problems:\nf(x) = (-2 * (x^-1)) + (x^-2) + (x^2) - (4 * x) + 8");
		System.out.println("f'(x) = (2 * (x^-2)) - (2 * (x^-3)) + (2 * x) - 4");
		System.out.println("\nx = " + x + "\naccuracy = " + accuracy + "\n");
		n.calculate(x);
		System.out.println("\n========================================\n");
	}

}
